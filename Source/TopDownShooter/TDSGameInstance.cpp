// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;
	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - Table doesn't exist - NULL"));
	}

	return bIsFind;
}

bool UTDSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;
	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		TArray<FName> RowNames = DropItemInfoTable->GetRowNames();
		for (int32 i = 0; i < RowNames.Num() && !bIsFind; i++)
		{
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = *DropItemInfoRow;
				bIsFind = true;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetDropItemInfoByName - Table doesn't exist - NULL"));
	}

	return bIsFind;
}

bool UTDSGameInstance::GetPickUpItemInfoByName(EWeaponType WeaponType, FDropPickUp& OutInfo)
{
	bool bIsFind = false;
	if (DropPickUpInfoTable)
	{
		FDropPickUp* DropPickUpInfoRow;
		TArray<FName> RowNames = DropPickUpInfoTable->GetRowNames();
		for (int32 i = 0; i < RowNames.Num() && !bIsFind; i++)
		{
			DropPickUpInfoRow = DropPickUpInfoTable->FindRow<FDropPickUp>(RowNames[i], "");
			if (DropPickUpInfoRow->WeaponType == WeaponType)
			{
				OutInfo = *DropPickUpInfoRow;
				bIsFind = true;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetPickUpItemInfoByName - Table doesn't exist - NULL"));
	}

	return bIsFind;
}
