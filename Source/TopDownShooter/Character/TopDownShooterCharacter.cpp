﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "../TDSGameInstance.h"
#include "Engine/World.h"


ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());


	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UTDSCharacterHealthComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATopDownShooterCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATopDownShooterCharacter::InitWeapon);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	Wearness = MaxWearness;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	//if (CursorToWorld != nullptr)
	//{
	//	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	//	{
	//		if (UWorld* World = GetWorld())
	//		{
	//			FHitResult HitResult;
	//			FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
	//			FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
	//			FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
	//			Params.AddIgnoredActor(this);
	//			World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
	//			FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
	//			CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
	//		}
	//	}
	//	else if (APlayerController* PC = Cast<APlayerController>(GetController()))
	//	{
	//		FHitResult TraceHitResult;
	//		PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	//		FVector CursorFV = TraceHitResult.ImpactNormal;
	//		FRotator CursorR = CursorFV.Rotation();
	//		CursorToWorld->SetWorldLocation(TraceHitResult.Location);
	//		CursorToWorld->SetWorldRotation(CursorR);
	//	}
	//}

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
	WearnessFun();
}

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName, );

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}



}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwicthNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwitchPreviosWeapon);
}

void ATopDownShooterCharacter::InputAxisX(float Value)
{
	if(!SprintRunEnabled && !SpendWearness)
		AxisX = Value;
}

void ATopDownShooterCharacter::InputAxisY(float Value)
{
	if (!SprintRunEnabled && !SpendWearness)
		AxisY = Value;
}

void ATopDownShooterCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATopDownShooterCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATopDownShooterCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}


void ATopDownShooterCharacter::WearnessFun()
{
	if (SpendWearness)
	{
		Wearness = Wearness - MinusWearness;
		if (Wearness <= 0)
		{
			SprintRunEnabled = false;
			SpendWearness = false;
			ChangeMovementState();

		}
	}
	else if(Wearness<=MaxWearness)
	{
			Wearness = Wearness + AddWearness;
			if (Wearness > MaxWearness)
				Wearness = MaxWearness;
	}
}

void ATopDownShooterCharacter::SprintFun(FVector HitTargetLocation)
{
	FVector vSprint = UKismetMathLibrary::GetDirectionUnitVector(GetActorLocation(), HitTargetLocation);
	FVector2D vSprint2 = FVector2D(vSprint);
	vSprint2 = UKismetMathLibrary::Normal2D(vSprint2*1000.f);
	FVector2D vAxis = UKismetMathLibrary::Normal2D(FVector2D(AxisX, AxisY)*1000.f);

	//движение в спринте возможно только по направлению взгляда
	SpendWearness = true;
	float Angle = UKismetMathLibrary::Acos(UKismetMathLibrary::Vector_CosineAngle2D(FVector(vSprint2,0.f), FVector(vAxis, 0.f)));

	if (Angle <= MaxSprintDegAngle * UKismetMathLibrary::GetPI() / 180)
	{
		//вычисление скорости для ускорения в спринте
		if (CurrentSprintTicks < MaxSpeedSprintTicks)
			CurrentSprintTicks += SprintDeltaTicks;
		else
			CurrentSprintTicks = MaxSpeedSprintTicks;


		if (SpendWearness)
			GetCharacterMovement()->MaxWalkSpeed = MovementInfo.SprintSpeed + 
			(MovementInfo.MaxSprintSpeed - MovementInfo.SprintSpeed) * (CurrentSprintTicks / MaxSpeedSprintTicks);
	}
	else
	{
		SprintRunEnabled = false;
		SpendWearness = false;
		ChangeMovementState();
	}

}

void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult resultHit;
		//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, resultHit);
		//направляет resultHit по тресу до прицела
		myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, resultHit);

		float FindRotatorResYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), resultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResYaw, 0.0f)));

		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);

			switch (MovementState)
			{
			case EMovementState::Aim_State:
				Displacement = FVector(0.f, 0.f, 160.f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::AimWalk_State:
				Displacement = FVector(0.f, 0.f, 160.f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::Walk_State:
				Displacement = FVector(0.f, 0.f, 120.f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::Run_State:
				Displacement = FVector(0.f, 0.f, 120.f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::Sprint_State:

				break;
			default:
				break;
			}
			CurrentWeapon->ShootEndLocation = resultHit.Location + Displacement;
		}

		if (SprintRunEnabled)
		{
			SprintFun(resultHit.Location);
		}
	}

	//добавляет вектор движения
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	/*if (CurrentWeapon)
	{
		if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
			CurrentWeapon->ShouldReduceDispersion = true;
		else
			CurrentWeapon->ShouldReduceDispersion = false;
	}*/
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float resSpeed = 600.f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		resSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimWalk_State:
		resSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Sprint_State:
		resSpeed = MovementInfo.SprintSpeed;
		break;
	case EMovementState::Walk_State:
		resSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		resSpeed = MovementInfo.RunSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = resSpeed;
}

void ATopDownShooterCharacter::ChangeMovementState()
{
	if (!SprintRunEnabled && !WalkEnabled && !AimEnabled)
	{
		SpendWearness = false;
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			SpendWearness = true;

			CurrentSprintTicks = 0.f;
			MovementState = EMovementState::Sprint_State;
		}
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}

	CharacterUpdate();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
		myWeapon->UpdateStateWeapon(MovementState);
}



void ATopDownShooterCharacter::InitWeapon(FName idWeapon, FAdditionalWeaponInfo AdditionalWeaponInfo, int32 NewIndexWeapon)//ToDo Init by id row by table
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		//myWeaponInfo = myGI->GetWeaponInfoByName(idWeapon);
		if (myGI->GetWeaponInfoByName(idWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->AdditionalWeaponInfo = AdditionalWeaponInfo;
					CurrentIndexWeapon = NewIndexWeapon;
					//UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - AdditionalWeaponInfo.Round = %i"), myWeapon->AdditionalWeaponInfo.Round);
					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					//debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);	   

					//if (InventoryComponent)
					//	CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(idWeapon);

					myWeapon->WeaponInit();
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponFireStart);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
				
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - WeaponInfo not found in table -NULL"));
			
		}	
	}

	
}

AWeaponDefault* ATopDownShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

bool ATopDownShooterCharacter::WeaponInitialized()
{
	return CurrentWeapon != NULL;
}

void ATopDownShooterCharacter::TryReloadWeapon()
{
	if(CurrentWeapon && !CurrentWeapon->WeaponReloading)
		if(CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
}

void ATopDownShooterCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATopDownShooterCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);

	}
	WeaponReloadEnd_BP(bIsSuccess, AmmoSafe);
}

void ATopDownShooterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATopDownShooterCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess, int32 AmmoSafe)
{
}

void ATopDownShooterCharacter::WeaponFireStart()
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	//if(Anim)
	//	WeaponFireStart_BP(Anim);
}

void ATopDownShooterCharacter::WeaponFireStart_BP_Implementation()
{
	// in BP
}

void ATopDownShooterCharacter::TrySwicthNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void ATopDownShooterCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

UDecalComponent* ATopDownShooterCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

float ATopDownShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float actualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	CharHealthComponent->ChangeHealthValue(-DamageAmount);

	return actualDamage;
}

void ATopDownShooterCharacter::CharDead()
{
	float animLength = 0.f;
	int32 rnd = FMath::RandHelper(DeadAnim.Num());

	if (DeadAnim.IsValidIndex(rnd) && DeadAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		animLength = DeadAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadAnim[rnd]);
	}
	UnPossessed();

	GetWorldTimerManager().SetTimer(TimerHandle_RagdollTimer, this, &ATopDownShooterCharacter::EnableRagdoll, animLength, false);
}

void ATopDownShooterCharacter::EnableRagdoll()
{
	UE_LOG(LogTemp, Error, TEXT(" ATopDownShooterCharacter::EnableRagdoll INIT"));
}
