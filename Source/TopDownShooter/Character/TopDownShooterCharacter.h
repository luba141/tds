﻿// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLibrary/Types.h"
#include "../WeaponDefault.h"
#include "TDSInventoryComponent.h"
#include "TDSCharacterHealthComponent.h"
#include "TopDownShooterCharacter.generated.h"


UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:

	ATopDownShooterCharacter();

	FTimerHandle TimerHandle_RagdollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;


	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UTDSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UTDSCharacterHealthComponent* CharHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
	//курсор
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	//движение
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	//статус движения
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool IsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadAnim;
	//усталость (спринт)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxWearness;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AddWearness;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MinusWearness;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
		float Wearness;
	//время спринта (для ускорения)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxSpeedSprintTicks;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintDeltaTicks;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxSprintDegAngle = 0;

	//Оружие	
	AWeaponDefault* CurrentWeapon = nullptr;

	//for demo 
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	//	TSubclassOf<AWeaponDefault> InitWeaponClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	UDecalComponent* CurrentCursor = nullptr;

	//ввод
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void WearnessFun();
	UFUNCTION()
		void SprintFun(FVector HitTargetLocation);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	float CurrentSprintTicks = 0.0f;
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	bool SpendWearness = false;
	bool AxisSprintClean = false; //флажок очистки переменных в случае, если курсор был слишком близко к персонажу во время спринта


	UFUNCTION()
		void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION()
		void InitWeapon(FName idWeapon, FAdditionalWeaponInfo AdditionaWeaponlInfo, int32 NewIndexWeapon);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		bool WeaponInitialized();

	UFUNCTION(BlueprintCallable)

	void TryReloadWeapon();
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION()
	void WeaponFireStart();
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP();


	//Inventory Func

	void TrySwicthNextWeapon();
	void TrySwitchPreviosWeapon();

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION()
	void CharDead();
	void EnableRagdoll();
};

