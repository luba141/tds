// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharacterHealthComponent.h"

void UTDSCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	Super::ChangeHealthValue(ChangeValue);
}

void UTDSCharacterHealthComponent::ReceiveDamage(float Damage)
{
	Super::ReceiveDamage(Damage);
}
