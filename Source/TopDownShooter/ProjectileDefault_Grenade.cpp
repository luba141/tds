// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

int32 DebugExplodeShow = 1;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TDS.DebugExplode"), DebugExplodeShow, TEXT("Drow debug for explotion"), ECVF_Cheat);

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);

	
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}



void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;

	if (DebugExplodeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Green, false, 12.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), (ProjectileSetting.ProjectileMinRadiusDamage + ProjectileSetting.ProjectileMaxRadiusDamage)/2, 12, FColor::Blue, false, 12.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Red, false, 12.f);
	}

	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}

	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMinDamage,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.ExplodeFallofCoef,
		NULL, IgnoredActor, nullptr, nullptr);

	this->Destroy();
}

//float AProjectileDefault_Grenade::CalcDamage(float k, float dmg, float distance, float maxDis, float minDis)
//{
//	if (distance <= minDis)
//		return dmg;
//	else if (distance > minDis && distance <= maxDis)
//		return dmg * k;
//	else
//		return 0.f;
//}
